/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : musiclibrary

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2020-12-18 23:49:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admininfo`
-- ----------------------------
DROP TABLE IF EXISTS `admininfo`;
CREATE TABLE `admininfo` (
  `#` int(50) NOT NULL AUTO_INCREMENT,
  `loginid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`#`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admininfo
-- ----------------------------
INSERT INTO `admininfo` VALUES ('1', '20194057101', '123456');
INSERT INTO `admininfo` VALUES ('2', '20194057102', '123456');
INSERT INTO `admininfo` VALUES ('3', '20194057103', '123456');
INSERT INTO `admininfo` VALUES ('4', '20194057104', '123456');
INSERT INTO `admininfo` VALUES ('5', '20194057105', '123456');
INSERT INTO `admininfo` VALUES ('6', '20194057106', '123456');
INSERT INTO `admininfo` VALUES ('7', '20194057107', '123456');
INSERT INTO `admininfo` VALUES ('8', '20194057108', '123456');
INSERT INTO `admininfo` VALUES ('9', '20194057109', '123456');
INSERT INTO `admininfo` VALUES ('10', '20194057110', '123456');
INSERT INTO `admininfo` VALUES ('11', '20194057111', '123456');
INSERT INTO `admininfo` VALUES ('12', '20194057112', '123456');
INSERT INTO `admininfo` VALUES ('13', '20194057113', '123456');
INSERT INTO `admininfo` VALUES ('14', '20194057114', '123456');
INSERT INTO `admininfo` VALUES ('15', '20194057115', '123456');
INSERT INTO `admininfo` VALUES ('16', '20194057116', '123456');
INSERT INTO `admininfo` VALUES ('17', '20194057117', '123456');
INSERT INTO `admininfo` VALUES ('18', '20194057118', '123456');
INSERT INTO `admininfo` VALUES ('19', '20194057119', '123456');
INSERT INTO `admininfo` VALUES ('20', '20194057120', '123456');
INSERT INTO `admininfo` VALUES ('21', '20194057121', '123456');
INSERT INTO `admininfo` VALUES ('22', '20194057122', '123456');
INSERT INTO `admininfo` VALUES ('23', '20194057123', '123456');
INSERT INTO `admininfo` VALUES ('24', '20194057124', '123456');
INSERT INTO `admininfo` VALUES ('25', '20194057125', '123456');
INSERT INTO `admininfo` VALUES ('26', '20194057126', '123456');
INSERT INTO `admininfo` VALUES ('27', '20194057127', '123456');
INSERT INTO `admininfo` VALUES ('28', '20194057128', '123456');
INSERT INTO `admininfo` VALUES ('29', '20194057129', '123456');

-- ----------------------------
-- Table structure for `ktvchinamusic`
-- ----------------------------
DROP TABLE IF EXISTS `ktvchinamusic`;
CREATE TABLE `ktvchinamusic` (
  `musicId` int(11) NOT NULL AUTO_INCREMENT,
  `musicName` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `musicAuthor` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `musicType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `musicDate` date DEFAULT NULL,
  PRIMARY KEY (`musicId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ktvchinamusic
-- ----------------------------
INSERT INTO `ktvchinamusic` VALUES ('1', '匆匆那年', '王菲-那英', '悲伤、舒缓', '2018-12-31');
INSERT INTO `ktvchinamusic` VALUES ('2', '夜明', '佐藤康夫', '柔和、感触', '2011-07-20');
INSERT INTO `ktvchinamusic` VALUES ('3', '左手指月', '萨顶顶', '激昂', '2018-07-27');
INSERT INTO `ktvchinamusic` VALUES ('4', '红色高跟鞋', '蔡健雅', '欢快', '2018-07-18');
INSERT INTO `ktvchinamusic` VALUES ('5', '北方有佳人', '晓旭', '诗词、悲伤', '2014-11-20');
INSERT INTO `ktvchinamusic` VALUES ('6', '来自天堂的魔鬼', '邓紫棋', '摇滚', '2017-11-13');
INSERT INTO `ktvchinamusic` VALUES ('7', '破茧', '张韶涵', '激情、昂扬', '2019-11-14');
INSERT INTO `ktvchinamusic` VALUES ('8', '一生所爱', '舒淇', '悲凉、伤感', '2019-05-13');
INSERT INTO `ktvchinamusic` VALUES ('9', '清风徐来', '王菲', '伤感、爱情', '2015-09-10');
INSERT INTO `ktvchinamusic` VALUES ('10', '时候', '刘美麟', '节奏、动感', '2019-09-13');

-- ----------------------------
-- Table structure for `ktvchineseopera`
-- ----------------------------
DROP TABLE IF EXISTS `ktvchineseopera`;
CREATE TABLE `ktvchineseopera` (
  `musicId` int(11) NOT NULL AUTO_INCREMENT,
  `musicName` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `musicAuthor` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `musicType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `musicDate` date DEFAULT NULL,
  PRIMARY KEY (`musicId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ktvchineseopera
-- ----------------------------
INSERT INTO `ktvchineseopera` VALUES ('1', '归去来兮', '曼珠', '戏音、欢快', '2019-06-20');
INSERT INTO `ktvchineseopera` VALUES ('2', '新贵妃醉酒', '李玉刚', '伤感', '2013-07-19');
INSERT INTO `ktvchineseopera` VALUES ('3', '梁山伯与祝英台', '方雪雯', '古代、悲感', '2000-06-23');
INSERT INTO `ktvchineseopera` VALUES ('4', '穆桂英挂帅', '马金凤', '豪爽、士气', '2005-07-22');
INSERT INTO `ktvchineseopera` VALUES ('5', '天上掉下个林妹妹', '钱惠丽', '开心、愉快', '2010-07-24');
INSERT INTO `ktvchineseopera` VALUES ('6', '我要你的爱', '梅艳芳', '淘气、性感', '2011-07-15');
INSERT INTO `ktvchineseopera` VALUES ('7', '夫妻双双把家还', '严凤英', '故事', '2009-11-13');

-- ----------------------------
-- Table structure for `ktvenglishmusic`
-- ----------------------------
DROP TABLE IF EXISTS `ktvenglishmusic`;
CREATE TABLE `ktvenglishmusic` (
  `musicId` int(11) NOT NULL AUTO_INCREMENT,
  `musicName` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `musicAuthor` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `musicType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `musicDate` date DEFAULT NULL,
  PRIMARY KEY (`musicId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ktvenglishmusic
-- ----------------------------
INSERT INTO `ktvenglishmusic` VALUES ('1', 'Mercy', 'ShawnMendes', 'Rock-Deluxe', '2016-11-10');
INSERT INTO `ktvenglishmusic` VALUES ('2', 'Youth', 'ShawnMendes', 'Remix', '2014-11-05');
INSERT INTO `ktvenglishmusic` VALUES ('3', 'Summerfire', 'B-U', 'Radio-Edit', '2004-03-25');
INSERT INTO `ktvenglishmusic` VALUES ('4', 'Dreamer', 'AishaBadru', 'Explicit', '2018-04-27');
INSERT INTO `ktvenglishmusic` VALUES ('5', 'Salt', 'AvaMax', 'Radio', '2019-12-12');
INSERT INTO `ktvenglishmusic` VALUES ('7', 'MysteryOfLove', 'SufjanStevens', 'Love-Sweet', '2017-12-01');
INSERT INTO `ktvenglishmusic` VALUES ('8', 'DanceMonkey', 'KidzBop', 'Remix', '2020-04-03');
INSERT INTO `ktvenglishmusic` VALUES ('9', 'DanceToThis', 'TroyeSivan', 'Little-Fresh', '2018-06-16');

-- ----------------------------
-- Table structure for `ktvplaylist`
-- ----------------------------
DROP TABLE IF EXISTS `ktvplaylist`;
CREATE TABLE `ktvplaylist` (
  `musicId` int(11) NOT NULL,
  `musicName` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `musicAuthor` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `musicType` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `musicDate` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`musicName`,`musicId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ktvplaylist
-- ----------------------------
INSERT INTO `ktvplaylist` VALUES ('8', '一生所爱', '舒淇', '悲凉、伤感', '2019-05-13');
INSERT INTO `ktvplaylist` VALUES ('6', '来自天堂的魔鬼', '邓紫棋', '摇滚', '2017-11-13');
INSERT INTO `ktvplaylist` VALUES ('7', '破茧', '张韶涵', '激情、昂扬', '2019-11-14');

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `#` int(50) NOT NULL AUTO_INCREMENT,
  `loginid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`#`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('1', '20194057101', '123456');
INSERT INTO `userinfo` VALUES ('2', '20194057102', '123456');
INSERT INTO `userinfo` VALUES ('3', '20194057103', '123456');
INSERT INTO `userinfo` VALUES ('4', '20194057104', '123456');
INSERT INTO `userinfo` VALUES ('5', '20194057105', '123456');
INSERT INTO `userinfo` VALUES ('6', '20194057106', '123456');
INSERT INTO `userinfo` VALUES ('7', '20194057107', '123456');
INSERT INTO `userinfo` VALUES ('8', '20194057108', '123456');
INSERT INTO `userinfo` VALUES ('9', '20194057109', '123456');
INSERT INTO `userinfo` VALUES ('10', '20194057110', '123456');
INSERT INTO `userinfo` VALUES ('11', '20194057111', '123456');
INSERT INTO `userinfo` VALUES ('12', '20194057112', '123456');
INSERT INTO `userinfo` VALUES ('13', '20194057113', '123456');
INSERT INTO `userinfo` VALUES ('14', '20194057114', '123456');
INSERT INTO `userinfo` VALUES ('15', '20194057115', '123456');
INSERT INTO `userinfo` VALUES ('16', '20194057116', '123456');
INSERT INTO `userinfo` VALUES ('17', '20194057117', '123456');
INSERT INTO `userinfo` VALUES ('18', '20194057118', '123456');
INSERT INTO `userinfo` VALUES ('19', '20194057119', '123456');
INSERT INTO `userinfo` VALUES ('20', '20194057120', '123456');
INSERT INTO `userinfo` VALUES ('21', '20194057121', '123456');
INSERT INTO `userinfo` VALUES ('22', '20194057122', '123456');
INSERT INTO `userinfo` VALUES ('23', '20194057123', '123456');
INSERT INTO `userinfo` VALUES ('24', '20194057124', '123456');
INSERT INTO `userinfo` VALUES ('25', '20194057125', '123456');
INSERT INTO `userinfo` VALUES ('26', '20194057126', '123456');
INSERT INTO `userinfo` VALUES ('27', '20194057127', '123456');
INSERT INTO `userinfo` VALUES ('28', '20194057128', '123456');
INSERT INTO `userinfo` VALUES ('29', '20194057129', '123456');
