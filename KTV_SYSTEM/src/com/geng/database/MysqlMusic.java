package com.geng.database;

import java.awt.HeadlessException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class MysqlMusic {
	public ArrayList<MusicVO> selectall() {
    	try {
    		String sql="select * from ktvchinamusic union all select * from ktvchineseopera union all select * from ktvenglishmusic";
    		System.out.println(sql);
    		DBConnect db=new DBConnect();
    		ResultSet rs=db.exequery(sql);
    		ArrayList<MusicVO> s=new ArrayList<MusicVO>();
    		while(rs.next()) {
    			MusicVO ss=new MusicVO();
    			ss.setMname(rs.getString("musicName").toString());
    			ss.setMauthor(rs.getString("musicAuthor").toString());
    			ss.setMtype(rs.getString("musicType").toString());
    			ss.setMtime(rs.getString("musicDate").toString());
    			s.add(ss);
    		}
    		return s;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
	public ArrayList<MusicVO> selectbytype(String musictype) {
    	try {
    		String sql="select * from "+musictype;
    		System.out.println(sql);
    		DBConnect db=new DBConnect();
    		ResultSet rs=db.exequery(sql);
    		ArrayList<MusicVO> s=new ArrayList<MusicVO>();
    		while(rs.next()) {
    			MusicVO ss=new MusicVO();
    			ss.setMname(rs.getString("musicName").toString());
    			ss.setMauthor(rs.getString("musicAuthor").toString());
    			ss.setMtype(rs.getString("musicType").toString());
    			ss.setMtime(rs.getString("musicDate").toString());
    			s.add(ss);
    		}
    		return s;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
	public ArrayList<MusicVO> selectbycon(MusicVO s1) {
		String sql = new String();
		String sql1 = "select * from ktvenglishmusic where 1=1";
		String sql2 = "select * from ktvchinamusic where 1=1";
		String sql3 = "select * from ktvchineseopera where 1=1";
		if(s1.getTypebox().equals("中文歌曲")) {
			sql = sql2;
		}
		else if(s1.getTypebox().equals("英文歌曲")) {
			sql = sql1;
		}
		else if(s1.getTypebox().equals("中国戏剧")) {
			sql = sql3;
		}
		//
		if(!s1.getMname().equals("")) {
			sql1 = sql1 + " and musicName like '%"+s1.getMname()+"%'";
			sql2 = sql2 + " and musicName like '%"+s1.getMname()+"%'";
			sql3 = sql3 + " and musicName like '%"+s1.getMname()+"%'";
			sql = sql + " and musicName like '%"+s1.getMname()+"%'";
		}
		if(!s1.getMauthor().equals("")) {
			sql1 = sql1 + " and musicAuthor like '%"+s1.getMauthor()+"%'";
			sql2 = sql2 + " and musicAuthor like '%"+s1.getMauthor()+"%'";
			sql3 = sql3 + " and musicAuthor like '%"+s1.getMauthor()+"%'";
			sql = sql + " and musicAuthor like '%"+s1.getMauthor()+"%'";
		}
		String sqlall = new String();
		if(s1.getTypebox().equals("")) {
			sqlall = sql1 + " union all "+sql2+" union all "+sql3;
		}
		else
			sqlall = sql;
    	try {
    	System.out.println(sqlall);
    	DBConnect db=new DBConnect();
    	ResultSet rs=db.exequery(sqlall);
    	ArrayList<MusicVO> s=new ArrayList<MusicVO>();
		while(rs.next()) {
			MusicVO ss=new MusicVO();
			ss.setMname(rs.getString("musicName").toString());
			ss.setMauthor(rs.getString("musicAuthor").toString());
			ss.setMtype(rs.getString("musicType").toString());
			ss.setMtime(rs.getString("musicDate").toString());
			s.add(ss);
		}
		System.out.println(s);
		return s;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
	public boolean insertplaylist(MusicVO s) {
		String sql1 = "select musicId from ktvplaylist where musicname = '"+s.getMname()+"'";
		DBConnect db = new DBConnect();
		ResultSet rs=db.exequery(sql1);
		System.out.println(sql1);
		try {
			if(rs.next()==false) {
				String sql = "insert into ktvplaylist(musicId,musicName,musicAuthor,musicType,musicDate)values('"+s.getMid()+"','"+s.getMname()+"','"+s.getMauthor()+"','"+s.getMtype()+"','"+s.getMtime()+"')";
				System.out.println(sql);
				if(db.exeupdate(sql)>0)
					return true;
				else
					return false;
			}
			else
				JOptionPane.showMessageDialog(null, "该歌曲已存在！无法再次添加！");
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean saveplaylist(ArrayList<MusicVO> musichandle) {
		clearplaylist();
		DBConnect db = new DBConnect();
		for(int i=0;i<musichandle.size();i++) {
			String sql = "insert into ktvplaylist(musicId,musicName,musicAuthor,musicType,musicDate)values('"+i+1+"','"+musichandle.get(i).getMname()+"','"+musichandle.get(i).getMauthor()+"','"+musichandle.get(i).getMtype()+"','"+musichandle.get(i).getMtime()+"')";
			db.exeupdate(sql);
			System.out.println(sql);
		}
		return true;
	}
	public ArrayList<MusicVO> selectplaylist() {
    	try {
    		String sql="select * from ktvplaylist";
    		System.out.println(sql);
    		DBConnect db=new DBConnect();
    		ResultSet rs=db.exequery(sql);
    		ArrayList<MusicVO> s=new ArrayList<MusicVO>();
    		while(rs.next()) {
    			MusicVO ss=new MusicVO();
    			ss.setMname(rs.getString("musicName").toString());
    			ss.setMauthor(rs.getString("musicAuthor").toString());
    			ss.setMtype(rs.getString("musicType").toString());
    			ss.setMtime(rs.getString("musicDate").toString());
    			s.add(ss);
    		}
    		return s;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
	public boolean clearplaylist() {
		String sql = "delete from ktvplaylist";
		System.out.println(sql);
		DBConnect db=new DBConnect();
		if(db.exeupdate(sql) > 0)
			return true;
		else
			return false;
	}
}
