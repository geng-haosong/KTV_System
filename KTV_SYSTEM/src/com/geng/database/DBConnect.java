package com.geng.database;

import java.sql.*;
public class DBConnect {
	private Connection conn;
    private void getconn() {
    	try{
    		Class.forName("com.mysql.jdbc.Driver"); 
    		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/musiclibrary","root","root");
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    public ResultSet exequery(String sql) {
    	try{
    		getconn();
    		Statement stm=conn.createStatement();
    		ResultSet rs=stm.executeQuery(sql);
            return rs;
    	}catch(Exception e){
    		e.printStackTrace();
    		return null;
    	}
    }
    public int exeupdate(String sql) {
    	try{
    		getconn();
    		Statement stm=conn.createStatement();
    		return stm.executeUpdate(sql);
    	}catch(Exception e){
    		e.printStackTrace();
    		return 0;
    	}
    }
}


