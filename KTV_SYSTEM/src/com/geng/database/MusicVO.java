package com.geng.database;

public class MusicVO {
	private int mid;
	private String mname;
	private String mauthor;
	private String mtype;
	private String mtime;
	private String typebox;
	public String getTypebox() {
		return typebox;
	}
	public void setTypebox(String typebox) {
		this.typebox = typebox;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getMauthor() {
		return mauthor;
	}
	public void setMauthor(String mauthor) {
		this.mauthor = mauthor;
	}
	public String getMtype() {
		return mtype;
	}
	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	public String getMtime() {
		return mtime;
	}
	public void setMtime(String mtime) {
		this.mtime = mtime;
	}
	
}
