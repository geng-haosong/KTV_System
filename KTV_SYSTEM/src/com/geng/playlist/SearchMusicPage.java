package com.geng.playlist;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.geng.database.MusicVO;
import com.geng.database.MysqlMusic;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.border.CompoundBorder;

public class SearchMusicPage extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JTextField textmname;
	private JTextField textmauthor;
	private MusicVO m=new MusicVO();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchMusicPage frame = new SearchMusicPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SearchMusicPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 857, 458);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("\u6B4C\u66F2\u540D\u79F0\uFF1A");
		label.setFont(new Font("宋体", Font.PLAIN, 20));
		label.setBounds(14, 42, 105, 35);
		contentPane.add(label);
		textmname = new JTextField();
		textmname.setFont(new Font("宋体", Font.PLAIN, 20));
		textmname.setColumns(10);
		textmname.setBounds(105, 47, 144, 24);
		contentPane.add(textmname);
		
		JLabel label_1 = new JLabel("\u6B4C\u66F2\u4F5C\u8005\uFF1A");
		label_1.setFont(new Font("宋体", Font.PLAIN, 20));
		label_1.setBounds(263, 50, 105, 18);
		contentPane.add(label_1);
		
		textmauthor = new JTextField();
		textmauthor.setFont(new Font("宋体", Font.PLAIN, 20));
		textmauthor.setColumns(10);
		textmauthor.setBounds(357, 47, 144, 24);
		contentPane.add(textmauthor);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(14, 99, 800, 194);
		contentPane.add(scrollPane);
		
		MysqlMusic s=new MysqlMusic();
		ArrayList<MusicVO> slist=s.selectall();
		String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
		String[][] values=new String[slist.size()][5];
		for(int i=0;i<slist.size();i++) {
			values[i][0]=i+1+"";
			values[i][1]=slist.get(i).getMname()+"";
			values[i][2]=slist.get(i).getMauthor()+"";
			values[i][3]=slist.get(i).getMtype()+"";
			values[i][4]=slist.get(i).getMtime()+"";
		}
		table = new JTable(values,title);
		table.setBorder(new CompoundBorder());
		table.setFont(new Font("宋体", Font.PLAIN, 17));
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int r=table.getSelectedRow();
				if(table.getValueAt(r, 0).toString().equals(""))
				    m.setMid(0);
				else
					m.setMid(Integer.parseInt(table.getValueAt(r, 0).toString()));
				m.setMname(table.getValueAt(r, 1).toString());
				m.setMauthor(table.getValueAt(r, 2).toString());
				m.setMtype(table.getValueAt(r, 3).toString());
				m.setMtime(table.getValueAt(r, 4).toString());
				textmname.setText(m.getMname());
				textmauthor.setText(m.getMauthor());
			}
		});
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("\u6B4C\u66F2\u641C\u7D22");
		lblNewLabel.setFont(new Font("宋体", Font.BOLD, 22));
		lblNewLabel.setBounds(310, 13, 204, 25);
		contentPane.add(lblNewLabel);
		
		JLabel label_2 = new JLabel("\u6B4C\u66F2\u7C7B\u578B\uFF1A");
		label_2.setFont(new Font("宋体", Font.PLAIN, 20));
		label_2.setBounds(515, 50, 105, 18);
		contentPane.add(label_2);
		
		String[] tpyes = {"不限","中文歌曲","英文歌曲","中国戏剧"};
		JComboBox<Object> TypeBox = new JComboBox<Object>(tpyes);
		TypeBox.setFont(new Font("宋体", Font.PLAIN, 20));
		TypeBox.setBounds(609, 47, 113, 24);
		contentPane.add(TypeBox);
		System.out.print(tpyes.toString());
		
		JButton btnNewButton = new JButton("\u6DFB\u52A0\u81F3\u64AD\u653E\u5217\u8868");
		btnNewButton.setFont(new Font("宋体", Font.BOLD, 20));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MysqlMusic s=new MysqlMusic();
				boolean b = s.insertplaylist(m);
				if(b)
					JOptionPane.showMessageDialog(null, "添加成功！");
			}
		});
		btnNewButton.setBounds(78, 336, 188, 42);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("\u8FD4\u56DE\u66F2\u5E93\u5217\u8868");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MusicLibrary mlFrame=new MusicLibrary(); 
				mlFrame.setVisible(true);
				close();
			}
		});
		btnNewButton_1.setFont(new Font("宋体", Font.BOLD, 20));
		btnNewButton_1.setBounds(323, 336, 178, 42);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("\u8FD4\u56DE\u64AD\u653E\u5217\u8868");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PlaylistPage plFrame = new PlaylistPage();
				plFrame.setVisible(true);
				close();
			}
		});
		btnNewButton_2.setFont(new Font("宋体", Font.BOLD, 20));
		btnNewButton_2.setBounds(559, 336, 188, 42);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("\u641C \u7D22");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MysqlMusic s=new MysqlMusic();
				MusicVO sv=new MusicVO();
				if(textmname.getText().equals(""))
				    sv.setMname("");
				else {
					sv.setMname(textmname.getText());
				}
				if(textmauthor.getText().equals(""))
				    sv.setMauthor("");
				else {
					sv.setMauthor(textmauthor.getText());
				}
				String typebox = TypeBox.getSelectedItem().toString();
				System.out.println(typebox);
				if(typebox=="不限") {
					sv.setTypebox("");
				}else {
					sv.setTypebox(typebox);
				}
				ArrayList<MusicVO> slist=s.selectbycon(sv);
				String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
				String[][] values=new String[slist.size()][5];
				for(int i=0;i<slist.size();i++) {
					values[i][0]=i+1+"";
					values[i][1]=slist.get(i).getMname()+"";
					values[i][2]=slist.get(i).getMauthor()+"";
					values[i][3]=slist.get(i).getMtype()+"";
					values[i][4]=slist.get(i).getMtime()+"";
				}
				DefaultTableModel dtm=new DefaultTableModel(values,title);
				table.setModel(dtm);
			}
		});
		btnNewButton_3.setFont(new Font("宋体", Font.BOLD, 18));
		btnNewButton_3.setBounds(736, 47, 89, 27);
		contentPane.add(btnNewButton_3);
	}
	public void close() {
		//this.dispose();
		this.setVisible(false);
	}
}
