package com.geng.playlist;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.andrewx.home.LoginPage;
import com.geng.database.MusicVO;
import com.geng.database.MysqlMusic;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class PlaylistPage extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JTextField textmname;
	private JTextField textmidx;
	private JTextField textmcount;
	private int count = 0;
	private ArrayList<MusicVO> musichandle = new ArrayList<MusicVO>();
	private Handlemusiclist h = new Handlemusiclist();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlaylistPage frame = new PlaylistPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PlaylistPage() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1216, 727);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(19, 92, 1141, 396);
		contentPane.add(scrollPane);
		
		MysqlMusic s=new MysqlMusic();
		musichandle = s.selectplaylist();
		count = musichandle.size();
		String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
		String[][] values=new String[musichandle.size()][5];
		for(int i=0;i<musichandle.size();i++) {
			values[i][0]=i+1+"";
			values[i][1]=musichandle.get(i).getMname()+"";
			values[i][2]=musichandle.get(i).getMauthor()+"";
			values[i][3]=musichandle.get(i).getMtype()+"";
			values[i][4]=musichandle.get(i).getMtime()+"";
		}
		DefaultTableModel dtm=new DefaultTableModel(values,title);
		table = new JTable();
		table.setModel(dtm);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int r=table.getSelectedRow();
				textmidx.setText(table.getValueAt(r, 0).toString());
				textmname.setText(table.getValueAt(r, 1).toString());
			}
		});
		table.setFont(new Font("SimSun", Font.PLAIN, 18));
		table.setToolTipText("table");
		table.setBackground(SystemColor.inactiveCaptionBorder);
		table.setBounds(543, 0, -541, 251);
		scrollPane.setViewportView(table);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(4, 0, 1189, 30);
		contentPane.add(menuBar);
		
		JMenu mnNewMenu = new JMenu("\u8D26\u53F7\u7BA1\u7406");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("\u4FEE\u6539\u8D26\u6237\u4FE1\u606F");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Userinfomod infomodFrame=new Userinfomod(); 
				infomodFrame.setVisible(true);
				close();
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem exitusermeau = new JMenuItem("\u9000\u51FA\u767B\u5F55");
		exitusermeau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginPage LogFrame=new LoginPage(); 
				LogFrame.setVisible(true);
				close();
			}
		});
		
		mnNewMenu.add(exitusermeau);
		
		JLabel lblNewLabel = new JLabel("\u6B4C\u66F2\u540D\u79F0\uFF1A");
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 18));
		lblNewLabel.setBounds(450, 517, 98, 26);
		contentPane.add(lblNewLabel);
		
		textmname = new JTextField();
		textmname.setFont(new Font("宋体", Font.PLAIN, 18));
		textmname.setBounds(531, 517, 168, 26);
		contentPane.add(textmname);
		textmname.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("\u5F53\u524D\u5E8F\u53F7\uFF1A");
		lblNewLabel_4.setFont(new Font("宋体", Font.PLAIN, 18));
		lblNewLabel_4.setBounds(759, 517, 103, 26);
		contentPane.add(lblNewLabel_4);
		
		textmidx = new JTextField();
		textmidx.setBackground(SystemColor.window);
		textmidx.setFont(new Font("宋体", Font.PLAIN, 18));
		textmidx.setBounds(854, 517, 69, 26);
		contentPane.add(textmidx);
		textmidx.setColumns(10);
		textmidx.setHorizontalAlignment(JTextField.CENTER );
		
		JButton frontBto = new JButton("\u6B4C\u66F2\u524D\u7F6E");
		frontBto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int pos = 0;
				if(!textmidx.getText().equals("")) {
					pos = Integer.parseInt(textmidx.getText()) - 1;
					h.setBefore(musichandle,pos);
					if(pos > 0) {
						uptable();
					textmidx.setText(table.getValueAt(pos - 1, 0).toString());
					textmname.setText(table.getValueAt(pos - 1, 1).toString());
					}
				}else {
					JOptionPane.showMessageDialog(null, "请先选择您要操作的歌曲!");
				}
			}
		});
		frontBto.setFont(new Font("宋体", Font.BOLD, 20));
		frontBto.setBounds(89, 572, 130, 55);
		contentPane.add(frontBto);
		
		JButton topBto = new JButton("\u6B4C\u66F2\u7F6E\u9876");
		topBto.setFont(new Font("宋体", Font.BOLD, 20));
		topBto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int pos = 0;
				if(!textmidx.getText().equals("")) {
					pos = Integer.parseInt(textmidx.getText()) - 1;
					h.setTop(musichandle, pos);
					uptable();
					textmidx.setText(1+"");
					textmname.setText(table.getValueAt(0, 1).toString());
				}else {
					JOptionPane.showMessageDialog(null, "请先选择您要操作的歌曲!");
				}
				
			}
		});
		topBto.setBounds(305, 572, 130, 55);
		contentPane.add(topBto);
		
		JButton deleteBto = new JButton("\u5220\u9664\u6B4C\u66F2");
		deleteBto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//删除歌曲
				if(count > 0) {
					int pos = 0;
					if(!textmidx.getText().equals("")) {
						pos = Integer.parseInt(textmidx.getText()) - 1;
					}
					h.deleteMusic(musichandle,pos);
					count--;
					uptable();
					if(count == 0) {
						textmidx.setText("");
						textmname.setText("");
					}else {
						textmidx.setText(1+"");
						textmname.setText(table.getValueAt(0, 1).toString());
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "当前播放列表为空，无法进行删除操作！");
				}
			}
		});
		deleteBto.setFont(new Font("宋体", Font.BOLD, 20));
		deleteBto.setBounds(529, 571, 130, 56);
		contentPane.add(deleteBto);
		
		JButton clearBto = new JButton("\u6E05\u7A7A\u6B4C\u66F2");
		clearBto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MysqlMusic s=new MysqlMusic();
				if(JOptionPane.showConfirmDialog(null, "是否确认清空播放列表？")==JOptionPane.YES_OPTION) {
					if(s.clearplaylist()) {
						musichandle = s.selectplaylist();
						count = musichandle.size();
						uptable();
						textmidx.setText("");
						textmname.setText("");
						textmcount.setText(count+"");
						JOptionPane.showMessageDialog(null, "播放列表清空成功！");
					}
	                else
		               JOptionPane.showMessageDialog(null, "播放列表清空失败！");
				}
			}
		});
		clearBto.setFont(new Font("宋体", Font.BOLD, 20));
		clearBto.setBounds(958, 572, 130, 55);
		contentPane.add(clearBto);
		
		JButton addBto = new JButton("\u6DFB\u52A0\u6B4C\u66F2");
		addBto.setFont(new Font("宋体", Font.BOLD, 20));
		addBto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MysqlMusic s=new MysqlMusic();
				s.saveplaylist(musichandle);
				MusicLibrary mlFrame=new MusicLibrary(); 
				mlFrame.setVisible(true);
				close();
			}
		});
		addBto.setBounds(745, 571, 130, 56);
		contentPane.add(addBto);
		
		JLabel lblNewLabel_1 = new JLabel("KTV\u70B9\u6B4C\u7CFB\u7EDF");
		lblNewLabel_1.setFont(new Font("楷体", Font.PLAIN, 27));
		lblNewLabel_1.setBounds(498, 37, 161, 40);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u5F53\u524D\u64AD\u653E\u5217\u8868\u6B4C\u66F2\u603B\u6570\uFF1A");
		lblNewLabel_2.setFont(new Font("宋体", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(130, 517, 213, 26);
		contentPane.add(lblNewLabel_2);
		
		textmcount = new JTextField();
		textmcount.setFont(new Font("宋体", Font.PLAIN, 18));
		textmcount.setBounds(320, 517, 61, 26);
		contentPane.add(textmcount);
		textmcount.setColumns(10);
		textmcount.setText(count+"");
		textmcount.setHorizontalAlignment(JTextField.CENTER );
	}
	public void close() {
		//this.dispose();
		this.setVisible(false);
	}
	public void uptable() {
		String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
		String[][] values=new String[musichandle.size()][5];
		for(int i=0;i<musichandle.size();i++) {
			values[i][0]=i+1+"";
			values[i][1]=musichandle.get(i).getMname()+"";
			values[i][2]=musichandle.get(i).getMauthor()+"";
			values[i][3]=musichandle.get(i).getMtype()+"";
			values[i][4]=musichandle.get(i).getMtime()+"";
		}
		DefaultTableModel dtm=new DefaultTableModel(values,title);
		table.setModel(dtm);
	}
}

