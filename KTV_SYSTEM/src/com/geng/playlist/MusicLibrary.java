package com.geng.playlist;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.geng.database.MusicVO;
import com.geng.database.MysqlMusic;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.SystemColor;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.UIManager;

public class MusicLibrary extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JTextField textmname;
	private JTextField textmauther;
	private JTextField textmtype;
	private JTextField textmtime;
	private JTextField textmusiclistcount;
	private JTextField textaddmnum;
	private JTextField textmid;
	private MusicVO m=new MusicVO();
	private int addNum = 0;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusicLibrary frame = new MusicLibrary();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MusicLibrary() {
		setTitle("KTV\u66F2\u5E93");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1232, 692);
		contentPane = new JPanel();
		contentPane.setBackground(UIManager.getColor("Button.background"));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton chinamusicbto = new JButton("\u4E2D\u6587\u6B4C\u66F2");
		chinamusicbto.setFont(new Font("楷体", Font.BOLD, 25));
		chinamusicbto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//筛选中文歌曲
				textmid.setText("");
				textmname.setText("");
				textmauther.setText("");
				textmtype.setText("");
				textmtime.setText("");
				String type = "ktvchinamusic";
				MysqlMusic s=new MysqlMusic();
				ArrayList<MusicVO> slist=s.selectbytype(type);;
				String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
				String[][] values=new String[slist.size()][5];
				for(int i=0;i<slist.size();i++) {
					values[i][0]=i+1+"";
					values[i][1]=slist.get(i).getMname()+"";
					values[i][2]=slist.get(i).getMauthor()+"";
					values[i][3]=slist.get(i).getMtype()+"";
					values[i][4]=slist.get(i).getMtime()+"";
				}
				DefaultTableModel dtm=new DefaultTableModel(values,title);
				table.setModel(dtm);
			}
		});
		chinamusicbto.setBounds(19, 142, 142, 56);
		contentPane.add(chinamusicbto);
		
		JButton englishmusicbto = new JButton("\u82F1\u6587\u6B4C\u66F2");
		englishmusicbto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textmid.setText("");
				textmname.setText("");
				textmauther.setText("");
				textmtype.setText("");
				textmtime.setText("");
				String type = "ktvenglishmusic";
				MysqlMusic s=new MysqlMusic();
				ArrayList<MusicVO> slist=s.selectbytype(type);;
				String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
				String[][] values=new String[slist.size()][5];
				for(int i=0;i<slist.size();i++) {
					values[i][0]=i+1+"";
					values[i][1]=slist.get(i).getMname()+"";
					values[i][2]=slist.get(i).getMauthor()+"";
					values[i][3]=slist.get(i).getMtype()+"";
					values[i][4]=slist.get(i).getMtime()+"";
				}
				DefaultTableModel dtm=new DefaultTableModel(values,title);
				table.setModel(dtm);
			}
		});
		englishmusicbto.setFont(new Font("楷体", Font.BOLD, 25));
		englishmusicbto.setBounds(19, 233, 142, 56);
		contentPane.add(englishmusicbto);
		
		JButton chineseoperabto = new JButton("\u4E2D\u56FD\u620F\u5267");
		chineseoperabto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textmid.setText("");
				textmname.setText("");
				textmauther.setText("");
				textmtype.setText("");
				textmtime.setText("");
				String type = "ktvchineseopera";
				MysqlMusic s=new MysqlMusic();
				ArrayList<MusicVO> slist=s.selectbytype(type);;
				String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
				String[][] values=new String[slist.size()][5];
				for(int i=0;i<slist.size();i++) {
					values[i][0]=i+1+"";
					values[i][1]=slist.get(i).getMname()+"";
					values[i][2]=slist.get(i).getMauthor()+"";
					values[i][3]=slist.get(i).getMtype()+"";
					values[i][4]=slist.get(i).getMtime()+"";
				}
				DefaultTableModel dtm=new DefaultTableModel(values,title);
				table.setModel(dtm);
			}
		});
		chineseoperabto.setFont(new Font("楷体", Font.BOLD, 25));
		chineseoperabto.setBounds(19, 321, 142, 56);
		contentPane.add(chineseoperabto);
		
		JLabel lblNewLabel = new JLabel("\u66F2\u5E93\u5217\u8868");
		lblNewLabel.setFont(new Font("华文楷体", Font.BOLD, 30));
		lblNewLabel.setBounds(611, 12, 152, 50);
		contentPane.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(188, 73, 995, 411);
		contentPane.add(scrollPane);
		
		MysqlMusic s=new MysqlMusic();
		ArrayList<MusicVO> slist=s.selectall();
		String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
		String[][] values=new String[slist.size()][5];
		for(int i=0;i<slist.size();i++) {
			values[i][0]=i+1+"";
			values[i][1]=slist.get(i).getMname()+"";
			values[i][2]=slist.get(i).getMauthor()+"";
			values[i][3]=slist.get(i).getMtype()+"";
			values[i][4]=slist.get(i).getMtime()+"";
		}
		table = new JTable(values,title);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int r=table.getSelectedRow();
				textmid.setText(table.getValueAt(r, 0).toString());
				textmname.setText(table.getValueAt(r, 1).toString());
				textmauther.setText(table.getValueAt(r, 2).toString());
				textmtype.setText(table.getValueAt(r, 3).toString());
				textmtime.setText(table.getValueAt(r, 4).toString());
			}
		});
		table.setFont(new Font("宋体", Font.PLAIN, 18));
		table.setToolTipText("table");
		table.setBackground(SystemColor.info);
		table.setBounds(543, 0, -541, 251);
		
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel_1 = new JLabel("\u6B4C\u66F2\u540D\u79F0\uFF1A");
		lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(188, 504, 105, 35);
		contentPane.add(lblNewLabel_1);
		
		textmname = new JTextField();
		textmname.setColumns(10);
		textmname.setBackground(Color.WHITE);
		textmname.setFont(new Font("宋体", Font.PLAIN, 18));
		textmname.setBounds(277, 509, 140, 24);
		contentPane.add(textmname);
		textmname.setHorizontalAlignment(JTextField.CENTER );
		
		JLabel lblNewLabel_2 = new JLabel("\u6B4C\u66F2\u4F5C\u8005\uFF1A");
		lblNewLabel_2.setFont(new Font("宋体", Font.PLAIN, 20));
		lblNewLabel_2.setBounds(434, 504, 105, 35);
		contentPane.add(lblNewLabel_2);
		
		textmauther = new JTextField();
		textmauther.setFont(new Font("宋体", Font.PLAIN, 18));
		textmauther.setBounds(525, 509, 144, 24);
		contentPane.add(textmauther);
		textmauther.setColumns(10);
		textmauther.setHorizontalAlignment(JTextField.CENTER );
		
		JLabel label = new JLabel("\u6B4C\u66F2\u7C7B\u578B\uFF1A");
		label.setFont(new Font("宋体", Font.PLAIN, 20));
		label.setBounds(694, 504, 105, 35);
		contentPane.add(label);
		
		textmtype = new JTextField();
		textmtype.setFont(new Font("宋体", Font.PLAIN, 18));
		textmtype.setBounds(786, 509, 135, 24);
		contentPane.add(textmtype);
		textmtype.setColumns(10);
		textmtype.setHorizontalAlignment(JTextField.CENTER );
		
		ArrayList<MusicVO> mlist=s.selectplaylist();
		int count = mlist.size();
		
		JButton addtomusiclistbto = new JButton("\u6DFB\u52A0\u81F3\u64AD\u653E\u5217\u8868");
		addtomusiclistbto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MysqlMusic s=new MysqlMusic();
				if(textmid.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "请先选择您要添加的歌曲！");
				    m.setMid(0);
				}
				else {
					m.setMid(Integer.parseInt(textmid.getText()));
					m.setMname(textmname.getText());
					m.setMauthor(textmauther.getText());
					m.setMtype(textmtype.getText());
					m.setMtime(textmtime.getText());
					boolean b = s.insertplaylist(m);
					if(b) {
						addNum++;
						textaddmnum.setText(addNum+"");
						textmusiclistcount.setText(count+addNum+"");
						JOptionPane.showMessageDialog(null, "添加成功！");
					}
				}	
			}
		});
		addtomusiclistbto.setFont(new Font("华文楷体", Font.BOLD, 25));
		addtomusiclistbto.setBounds(406, 561, 217, 55);
		contentPane.add(addtomusiclistbto);
		
		JButton returnmusiclistbto = new JButton("\u8FD4\u56DE\u64AD\u653E\u5217\u8868");
		returnmusiclistbto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PlaylistPage plFrame = new PlaylistPage(); //返回歌曲播放列表
				plFrame.setVisible(true);
				close();
			}
		});
		returnmusiclistbto.setFont(new Font("华文楷体", Font.BOLD, 25));
		returnmusiclistbto.setBounds(757, 561, 207, 54);
		contentPane.add(returnmusiclistbto);
		
		JLabel label_1 = new JLabel("\u53D1\u884C\u65F6\u95F4\uFF1A");
		label_1.setFont(new Font("宋体", Font.PLAIN, 20));
		label_1.setBounds(943, 504, 105, 35);
		contentPane.add(label_1);
		
		textmtime = new JTextField();
		textmtime.setFont(new Font("宋体", Font.PLAIN, 18));
		textmtime.setBounds(1033, 509, 125, 24);
		contentPane.add(textmtime);
		textmtime.setColumns(10);
		textmtime.setHorizontalAlignment(JTextField.CENTER );
		
		JLabel label_2 = new JLabel("\u64AD\u653E\u5217\u8868\u6B4C\u66F2\u603B\u6570\uFF1A");
		label_2.setFont(new Font("宋体", Font.PLAIN, 20));
		label_2.setBounds(35, 557, 186, 27);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("\u672C\u6B21\u6DFB\u52A0\u6B4C\u66F2\u6570\u91CF\uFF1A");
		label_3.setFont(new Font("宋体", Font.PLAIN, 20));
		label_3.setBounds(33, 592, 185, 24);
		contentPane.add(label_3);
		
		textmusiclistcount = new JTextField();
		textmusiclistcount.setFont(new Font("宋体", Font.PLAIN, 18));
		textmusiclistcount.setBounds(211, 559, 45, 24);
		contentPane.add(textmusiclistcount);
		textmusiclistcount.setColumns(10);
		textmusiclistcount.setText(count+"");
		textmusiclistcount.setHorizontalAlignment(JTextField.CENTER );
		
		textaddmnum = new JTextField();
		textaddmnum.setFont(new Font("宋体", Font.PLAIN, 18));
		textaddmnum.setBounds(211, 594, 45, 24);
		contentPane.add(textaddmnum);
		textaddmnum.setColumns(10);
		textaddmnum.setText(addNum+"");
		textaddmnum.setHorizontalAlignment(JTextField.CENTER );
		
		JButton searchmusicbto = new JButton("\u6B4C\u66F2\u641C\u7D22");
		searchmusicbto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchMusicPage smFrame=new SearchMusicPage(); 
				smFrame.setVisible(true);
				close();
			}
		});
		searchmusicbto.setFont(new Font("楷体", Font.BOLD, 25));
		searchmusicbto.setBounds(19, 409, 142, 56);
		contentPane.add(searchmusicbto);
		
		JButton allmusicbto = new JButton("\u5168\u90E8\u6B4C\u66F2");
		allmusicbto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textmid.setText("");
				textmname.setText("");
				textmauther.setText("");
				textmtype.setText("");
				textmtime.setText("");
				MysqlMusic s=new MysqlMusic();
				ArrayList<MusicVO> slist=s.selectall();
				String[] title= {"序号","歌曲名称","作者","类型","发行时间"};
				String[][] values=new String[slist.size()][5];
				for(int i=0;i<slist.size();i++) {
					values[i][0]=i+1+"";
					values[i][1]=slist.get(i).getMname()+"";
					values[i][2]=slist.get(i).getMauthor()+"";
					values[i][3]=slist.get(i).getMtype()+"";
					values[i][4]=slist.get(i).getMtime()+"";
				}
				DefaultTableModel dtm=new DefaultTableModel(values,title);
				table.setModel(dtm);
			}
		});
		allmusicbto.setFont(new Font("楷体", Font.BOLD, 25));
		allmusicbto.setBounds(19, 53, 142, 56);
		contentPane.add(allmusicbto);
		
		textmid = new JTextField();
		textmid.setFont(new Font("宋体", Font.PLAIN, 18));
		textmid.setBounds(120, 509, 41, 24);
		contentPane.add(textmid);
		textmid.setColumns(10);
		textmid.setHorizontalAlignment(JTextField.CENTER );
		
		JLabel label_4 = new JLabel("\u5E8F\u53F7\uFF1A");
		label_4.setFont(new Font("宋体", Font.PLAIN, 20));
		label_4.setBounds(70, 504, 60, 35);
		contentPane.add(label_4);
	}
	public void close() {
		//this.dispose();
		this.setVisible(false);
	}
	
}
