package com.andrewx.ktv_system.handlemusic;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import com.andrewx.ktv_system.test.Test;
import com.andrewx.ktv_system.interfaces.Music_Interface;
import com.andrewx.ktv_system.musictype.*;

public class HandleMusic {
	public final  List<Music_Interface> musicList = new ArrayList<Music_Interface>();
	//addMusic
	public  void addMusic(List<EnglishMusic> enMusic,List<ChinaMusic> cnMusic,List<ChineseOpera> cnOpera) {
		System.out.println("①、英文歌曲          ②、中文歌曲          ③、戏曲");
		System.out.print("输入添加歌曲至曲库的种类:");
		Scanner sc = new Scanner(System.in);
		int cmd = sc.nextInt();
		switch(cmd) {
		case 1:
			EnglishMusic enm = new EnglishMusic(); 
			System.out.print("输入歌曲名称:");enm.setName(sc.next());
			System.out.print("输入作者姓名:");enm.setAuthor(sc.next());
			System.out.print("输入作曲日期:");enm.setSdate(sc.next());
			System.out.print("输入歌曲类型:");enm.setType(sc.next());
			enm.number=enMusic.size()+1;
			enMusic.add(enm);
			break;
		case 2:
			ChinaMusic cnm = new ChinaMusic(); 
			System.out.print("输入歌曲名称:");cnm.setName(sc.next());
			System.out.print("输入作者姓名:");cnm.setAuthor(sc.next());
			System.out.print("输入作曲日期:");cnm.setSdate(sc.next());
			System.out.print("输入歌曲类型:");cnm.setType(sc.next());
			cnm.number=cnMusic.size()+1;
			cnMusic.add(cnm);
			break;
		case 3:
			ChineseOpera cno = new ChineseOpera(); 
			System.out.print("输入戏曲名称:");cno.setName(sc.next());
			System.out.print("输入作者姓名:");cno.setAuthor(sc.next());
			System.out.print("输入作曲日期:");cno.setSdate(sc.next());
			System.out.print("输入歌曲类型:");cno.setType(sc.next());
			System.out.print("输入戏曲故事:");cno.setStory(sc.next());
			cno.number=cnOpera.size()+1;
			cnOpera.add(cno);
			break;
		default :
			System.out.println("Wrong choose.");
			break;
		}
	}
	//addMusicList
	public  void addMusicList(List<EnglishMusic> enMusic,List<ChinaMusic> cnMusic,List<ChineseOpera> cnOpera) {
		System.out.println("①、英文歌曲          ②、中文歌曲          ③、戏曲");
		System.out.print("输入添加歌曲至曲库的种类:");
		Scanner sc = new Scanner(System.in);
		int cmd = sc.nextInt();
		int num;
		switch(cmd) {
		case 1:
			System.out.println("英文歌曲列表汇总:");
			for(EnglishMusic obj : enMusic) {
				obj.show();
			}
			System.out.print("输入要添加的歌曲编号:");num = sc.nextInt();
			musicList.add(enMusic.get(num-1));
			System.out.println("歌曲添加完成!");
			break;
		case 2:
			System.out.println("中文歌曲列表汇总:");
			for(ChinaMusic obj : cnMusic) {
				obj.show();
			}
			System.out.print("输入要添加的歌曲编号:");num = sc.nextInt();
			musicList.add(cnMusic.get(num-1));
			System.out.println("歌曲添加完成!");
			break;
		case 3:
			System.out.println("戏曲列表汇总:");
			for(ChineseOpera obj : cnOpera) {
				obj.show();
			}
			System.out.print("输入要添加的歌曲编号:");num = sc.nextInt();
			musicList.add(cnOpera.get(num-1));
			System.out.println("歌曲添加完成!");
			break;
		default :
			System.out.println("Wrong choose.");
			break;
		}
		
	}
	//showMusicList
	public  void showMusicList() {
		System.out.println("当前歌单播放列表:");
		System.out.println("歌曲\t作者");
		for(Music_Interface obj : musicList) {
			if(obj instanceof EnglishMusic)
				((EnglishMusic) obj).showMusiclist();
			else if(obj instanceof ChinaMusic)
				((ChinaMusic) obj).showMusiclist();
			else
				((ChineseOpera) obj).showMusiclist();
		}
	}
	//deleteMusic
	public  void deleteMusic(List<EnglishMusic> enMusic,List<ChinaMusic> cnMusic,List<ChineseOpera> cnOpera) {
		System.out.println("①、英文歌曲          ②、中文歌曲          ③、戏曲");
		System.out.print("输入要清空的歌单:");
		Scanner sc = new Scanner(System.in);
		int cmd = sc.nextInt();
		switch(cmd) {
		case 1:
			if(!enMusic.isEmpty()) {
				System.out.println("当前列表为空!");
			}
			else {
				enMusic.clear();
				System.out.println("英文歌曲库置空成功!");
			}
			break;
		case 2:
			if(!cnMusic.isEmpty()) {
				System.out.println("当前列表为空!");
			}
			else {
				cnMusic.clear();
				System.out.println("中文歌曲库置空成功!");
			}
			break;
		case 3:
			if(!cnOpera.isEmpty()) {
				System.out.println("当前列表为空!");
			}
			else {
				cnOpera.clear();
				System.out.println("中华戏曲库置空成功!");
			}
			break;
		default :
			System.out.println("Wrong choose.");
			break;
		}
	}
	//clearMusic
	public  void clearMusic() {
		if(!musicList.isEmpty()) {
			System.out.println("当前列表为空!");
		}
		else {
			musicList.clear();
			System.out.println("播放列表置空成功!");
		}
	}
	//setBefore
	public  void setBefore() {
		showMusicList();
		System.out.print("请输入前置列表歌曲的名字:");
		Scanner sc = new Scanner(System.in);
		String musicSearch = sc.next();
		System.out.println("musicSearch = "+musicSearch);
		int pos=-1;
		for(int i=0;i<musicList.size();i++) {
			if(musicList.get(i) instanceof EnglishMusic) {
				if((((EnglishMusic) musicList.get(i)).getName()).equals(musicSearch)){
					pos = i;
					System.out.println("匹配正确");
					break;
				}
				else continue;
			}
			else if(musicList.get(i) instanceof ChinaMusic) {
				if((((ChinaMusic) musicList.get(i)).getName()).equals(musicSearch)){
					pos = i;
					System.out.println("匹配正确");
					break;
				}
				else continue;
			}
			else if(musicList.get(i) instanceof ChineseOpera) {
				if((((ChineseOpera) musicList.get(i)).getName()).equals(musicSearch)){
					pos = i;
					System.out.println("匹配正确");
					break;
				}
				else continue;
			}
		}
		if(pos==-1)
			System.out.println("列表中无该歌曲!");
		else if(pos==0)
			System.out.println("该首歌曲已经在最顶部,不用前置了!");
		else {
			musicList.add(pos-1, musicList.get(pos));
			musicList.remove(pos+1);
			System.out.println("歌曲前置成功!");
		}
	}
	//setTop
	public void setTop() {
		showMusicList();
		System.out.print("请输入置顶列表歌曲的名字:");
		Scanner sc = new Scanner(System.in);
		String musicSearch = sc.next();
		System.out.println("musicSearch = "+musicSearch);
		int pos=-1;
		for(int i=0;i<musicList.size();i++) {
			if(musicList.get(i) instanceof EnglishMusic) {
				if((((EnglishMusic) musicList.get(i)).getName()).equals(musicSearch)){
					pos = i;
					break;
				}
				else continue;
			}
			else if(musicList.get(i) instanceof ChinaMusic) {
				if((((ChinaMusic) musicList.get(i)).getName()).equals(musicSearch)){
					pos = i;
					break;
				}
				else continue;
			}
			else if(musicList.get(i) instanceof ChineseOpera) {
				if((((ChineseOpera) musicList.get(i)).getName()).equals(musicSearch)){
					pos = i;
					break;
				}
				else continue;
			}
		}
		if(pos==-1)
			System.out.println("列表中无该歌曲!");
		else if(pos==0)
			System.out.println("该首歌曲已经在最顶部,不用置顶了!");
		else {
			musicList.add(0, musicList.get(pos));
			musicList.remove(pos+1);
			System.out.println("歌曲置顶成功!");
		}
	}

}
