package com.andrewx.ktv_system.test;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.andrewx.ktv_system.handlemusic.HandleMusic;
import com.andrewx.ktv_system.musictype.ChinaMusic;
import com.andrewx.ktv_system.musictype.ChineseOpera;
import com.andrewx.ktv_system.musictype.EnglishMusic;
public class Test {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HandleMusic handle = new HandleMusic();
		List<EnglishMusic> enMusic = new ArrayList<EnglishMusic>();
		List<ChinaMusic> cnMusic = new ArrayList<ChinaMusic>();
		List<ChineseOpera> cnOpera = new ArrayList<ChineseOpera>();
		menu(enMusic,cnMusic,cnOpera,handle);
	}
	//
	public static void menu(List<EnglishMusic> enMusic,List<ChinaMusic> cnMusic,List<ChineseOpera> cnOpera,HandleMusic handle) {
		System.out.println("----------欢迎使用点歌台系统----------");
		System.out.println("1、添加歌曲曲库");
		System.out.println("2、添加歌曲至播放列表");
		System.out.println("3、清空歌曲曲库");
		System.out.println("4、清空歌曲播放列表");
		System.out.println("5、播放列表歌曲前移");
		System.out.println("6、歌曲置顶播放列表");
		System.out.println("7、播放列表");
		System.out.println("0、退出");
		while(true) {
			Scanner sc = new Scanner(System.in);
			System.out.print("请输入您要执行的操作选项:");
			int cmd = sc.nextInt();
			switch(cmd) {
			case 0:
				System.out.println("再见!");
				return ;
			case 1:
				handle.addMusic(enMusic,cnMusic,cnOpera);
				break;
			case 2:
				handle.addMusicList(enMusic,cnMusic,cnOpera);
				break;
			case 3:
				handle.deleteMusic(enMusic,cnMusic,cnOpera);
				break;
			case 4:
				handle.clearMusic();
				break;
			case 5:
				handle.setBefore();
				break;
			case 6:
				handle.setTop();
				break;
			case 7:
				handle.showMusicList();
				break;
			default :
				System.out.println("Wrong choose.");
				break;
			}
		}
	}

}
