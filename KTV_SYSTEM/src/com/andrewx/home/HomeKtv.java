package com.andrewx.home;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.beans.PropertyVetoException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JInternalFrame;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.Timer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import java.awt.Color;
import java.awt.Container;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;
import javax.swing.JRadioButtonMenuItem;

public class HomeKtv extends JFrame {

	private JPanel contentPane;
	JDesktopPane desktopPane = new JDesktopPane();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomeKtv frame = new HomeKtv();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HomeKtv() {
		setTitle("KTV\u70B9\u6B4C\u7BA1\u7406\u7CFB\u7EDFAdmin");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 937, 674);
		//设置menuBar菜单
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		//设置菜单
		JMenu homeFileCmd = new JMenu("\u6587\u4EF6\u9009\u9879");
		homeFileCmd.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 20));
		menuBar.add(homeFileCmd);
		
		JMenuItem newFile = new JMenuItem("\u65B0\u5EFA");
		newFile.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		homeFileCmd.add(newFile);
		
		JMenu openFileFrom = new JMenu("\u6253\u5F00\u6587\u4EF6");
		openFileFrom.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		homeFileCmd.add(openFileFrom);
		
		JMenuItem fromWorkspace = new JMenuItem("\u4ECE\u672C\u5730\u5DE5\u4F5C\u8DEF\u5F84\u5BFC\u5165");
		fromWorkspace.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		openFileFrom.add(fromWorkspace);
		
		JMenuItem formOutside = new JMenuItem("\u4ECE\u5916\u90E8\u5BFC\u5165");
		formOutside.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		openFileFrom.add(formOutside);
		
		JMenuItem save = new JMenuItem("\u4FDD\u5B58");
		save.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		homeFileCmd.add(save);
		
		JMenuItem saveTo = new JMenuItem("\u4FDD\u5B58\u5230");
		saveTo.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		homeFileCmd.add(saveTo);
		//
		JMenu dataHandle = new JMenu("\u6570\u636E\u64CD\u4F5C");
		dataHandle.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 20));
		menuBar.add(dataHandle);
		
		JMenu toMusicLibrary = new JMenu("\u66F2\u5E93");
		toMusicLibrary.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		dataHandle.add(toMusicLibrary);
		
		JMenuItem englishMusic = new JMenuItem("\u82F1\u6587\u6B4C\u66F2");
		englishMusic.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		englishMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EnglishMusicHandlePage enHandle = new EnglishMusicHandlePage();
				enHandle.setTitle("ktvenglishmusic");
				enHandle.setVisible(true);
				
				//desktopPane.add(enHandle);
			}
		});
		toMusicLibrary.add(englishMusic);
		
		JMenuItem chinaMusic = new JMenuItem("\u4E2D\u6587\u6B4C\u66F2");
		chinaMusic.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		chinaMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EnglishMusicHandlePage enHandle = new EnglishMusicHandlePage();
				enHandle.setTitle("ktvchinamusic");
				enHandle.setVisible(true);
				
				//desktopPane.add(enHandle);
			}
		});
		toMusicLibrary.add(chinaMusic);
		
		JMenuItem chineseOpera = new JMenuItem("\u4E2D\u534E\u620F\u66F2");
		chineseOpera.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
		chineseOpera.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EnglishMusicHandlePage enHandle = new EnglishMusicHandlePage();
				enHandle.setTitle("ktvchineseopera");
				enHandle.setVisible(true);
				
				//desktopPane.add(enHandle);
			}
		});
		toMusicLibrary.add(chineseOpera);
		getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(desktopPane, BorderLayout.CENTER);
		
		JPanel groundpanel = new JPanel();
		groundpanel.setForeground(Color.MAGENTA);
		groundpanel.setBounds(0, 0, 905, 577);
		desktopPane.add(groundpanel);
		groundpanel.setLayout(null);
		
		JLabel adminTitleName = new JLabel("\u6B22\u8FCE\u4F7F\u7528\u7BA1\u7406\u5458KTV\u7BA1\u7406\u7CFB\u7EDF");
		adminTitleName.setForeground(Color.PINK);
		adminTitleName.setFont(new Font("隶书", Font.BOLD, 60));
		adminTitleName.setBounds(44, 52, 817, 178);
		groundpanel.add(adminTitleName);
		
		JLabel groupMember = new JLabel("\u5C0F\u7EC4\u6210\u5458:");
		groupMember.setFont(new Font("SimSun", Font.PLAIN, 18));
		groupMember.setForeground(Color.MAGENTA);
		groupMember.setBounds(59, 329, 81, 21);
		groundpanel.add(groupMember);
		
		JLabel groupMemberDetail = new JLabel("\u8C22\u5C0F\u9F99\u3001\u803F\u7693\u677E\u3001\u5F20\u660E\u4EAE\u3001\u6768\u8F89");
		groupMemberDetail.setFont(new Font("SimSun", Font.PLAIN, 18));
		groupMemberDetail.setForeground(Color.MAGENTA);
		groupMemberDetail.setBounds(167, 329, 290, 21);
		groundpanel.add(groupMemberDetail);
		
		JLabel manual = new JLabel("\u5E94\u7528\u4F7F\u7528\u8BF4\u660E\uFF1A");
		manual.setFont(new Font("SimSun", Font.PLAIN, 18));
		manual.setForeground(Color.MAGENTA);
		manual.setBounds(59, 375, 126, 21);
		groundpanel.add(manual);
		
		JLabel manualDetail = new JLabel("\u8BF7\u8BE6\u7EC6\u67E5\u770BDescribe\u6587\u4EF6");
		manualDetail.setFont(new Font("SimSun", Font.PLAIN, 18));
		manualDetail.setForeground(Color.MAGENTA);
		manualDetail.setBounds(191, 375, 250, 21);
		groundpanel.add(manualDetail);
		
		JPanel timePanel = new JPanel();
		timePanel.setBounds(401, 472, 460, 90);
		groundpanel.add(timePanel);
		
		//time
		class MyTime implements Runnable {
			private static final long serialVersionUID = 1L;
			JLabel MyJLabel = new JLabel();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
			public MyTime() {		
				Date date  =  new Date();
				MyJLabel = new JLabel(sdf.format(date));
				MyJLabel.setForeground(Color.DARK_GRAY);
				MyJLabel.setFont(new Font("幼圆", Font.PLAIN, 20));
				MyJLabel.setBounds(233, 36, 250, 15);
				timePanel.add(MyJLabel);
			}
			public void run() {
				while (true) {
					try {
						Thread.sleep(100);
						MyJLabel.setText(sdf.format(new Date()));
					} catch (InterruptedException e) {
						e.printStackTrace();
						}
					}
			}
		}
		//
		MyTime t = new MyTime();
		 new Thread(t).start();
		 
	}
	

}
