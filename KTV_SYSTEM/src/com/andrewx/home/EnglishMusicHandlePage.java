package com.andrewx.home;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import com.andrewx.ktv_system.musictype.EnglishMusic;
import com.geng.database.AdminHandleJDBC;
import com.geng.database.MusicVO;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Font;

public class EnglishMusicHandlePage extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JScrollPane jsp;
	private JTable tableInfo;
	private DefaultTableModel dtm;
	private JTextField txtEnMusicName;
	private JTextField txtEnMusicAuthor;
	private JTextField txtEnMusicDate;
	private JTextField txtEnMusicType;
	private JTextField txtChangeName;
	private JTextField txtChangeAuthor;
	private JTextField txtChangeDate;
	private JTextField txtChangeType;
	private JTextField txtDelName;
	private JTextField txtDelAuthor;
	private JTextField txtDelDate;
	private JTextField txtDelType;
	private JTextField txtSearchName;
	private JTextField txtSearchAuthor;
	private JTextField txtSearchDate;
	private JTextField txtSearchType;
	public Integer musicID;
	public static String titleType;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EnglishMusicHandlePage frame = new EnglishMusicHandlePage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
//	class titletype{
//		private String titleType;
//
//		public String getTitleType() {
//			return titleType;
//		}
//
//		public void setTitleType(String titleType) {
//			this.titleType = titleType;
//		}
//		
//	}
	
	
	public EnglishMusicHandlePage() {
		
		//setTitle("ktvenglishmusic");
		setBounds(100, 100, 1111, 676);
		getContentPane().setLayout(null);
		
		JPanel groundPanel = new JPanel();
		groundPanel.setBackground(SystemColor.menu);
		groundPanel.setBounds(0, 0, 1100, 624);
		getContentPane().add(groundPanel);
		groundPanel.setLayout(null);
		//
		
		JPanel showTablePane = new JPanel();
		showTablePane.setBounds(196, 15, 878, 398);
		groundPanel.add(showTablePane);
		showTablePane.setLayout(null);
		
		tableInfo = new JTable();
		tableInfo.setFont(new Font("SimSun", Font.PLAIN, 18));
		tableInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int r=tableInfo.getSelectedRow();
				musicID = Integer.parseInt(tableInfo.getValueAt(r, 1).toString());//获取musicId
				txtSearchName.setText(tableInfo.getValueAt(r, 2).toString());
				txtSearchAuthor.setText(tableInfo.getValueAt(r, 3).toString());
				txtSearchDate.setText(tableInfo.getValueAt(r, 5).toString());
				txtSearchType.setText(tableInfo.getValueAt(r, 4).toString());
				txtDelName.setText(tableInfo.getValueAt(r, 2).toString());
				txtDelAuthor.setText(tableInfo.getValueAt(r, 3).toString());
				txtDelDate.setText(tableInfo.getValueAt(r, 5).toString());
				txtDelType.setText(tableInfo.getValueAt(r, 4).toString());
				txtChangeName.setText(tableInfo.getValueAt(r, 2).toString());
				txtChangeAuthor.setText(tableInfo.getValueAt(r, 3).toString());
				txtChangeDate.setText(tableInfo.getValueAt(r, 5).toString());
				txtChangeType.setText(tableInfo.getValueAt(r, 4).toString());
			}
		});
		tableInfo.setBounds(543, 0, -541, 251);
		showTablePane.add(tableInfo);
		
		jsp = new JScrollPane(tableInfo);
		jsp.setBounds(0, 0, 878, 398);
		showTablePane.add(jsp);
		
		JPanel searchHandlePanel = new JPanel();
		searchHandlePanel.setLayout(null);
		searchHandlePanel.setBounds(196, 421, 878, 188);
		groundPanel.add(searchHandlePanel);
		
		JLabel searchEnMusicNameLabel = new JLabel("\u6B4C\u66F2\u540D\u79F0:");
		searchEnMusicNameLabel.setFont(new Font("宋体", Font.PLAIN, 18));
		searchEnMusicNameLabel.setBounds(113, 38, 100, 21);
		searchHandlePanel.add(searchEnMusicNameLabel);
		
		txtSearchName = new JTextField();
		txtSearchName.setFont(new Font("SimSun", Font.PLAIN, 18));
		txtSearchName.setColumns(10);
		txtSearchName.setBounds(201, 35, 150, 27);
		searchHandlePanel.add(txtSearchName);
		
		JLabel searchEnMusicAuthorLabel = new JLabel("\u4F5C    \u8005:");
		searchEnMusicAuthorLabel.setFont(new Font("宋体", Font.PLAIN, 18));
		searchEnMusicAuthorLabel.setBounds(570, 41, 90, 21);
		searchHandlePanel.add(searchEnMusicAuthorLabel);
		
		txtSearchAuthor = new JTextField();
		txtSearchAuthor.setFont(new Font("SimSun", Font.PLAIN, 18));
		txtSearchAuthor.setColumns(10);
		txtSearchAuthor.setBounds(656, 38, 150, 27);
		searchHandlePanel.add(txtSearchAuthor);
		
		JLabel searchEnMusicDateLabel = new JLabel("\u65E5    \u671F:");
		searchEnMusicDateLabel.setFont(new Font("宋体", Font.PLAIN, 18));
		searchEnMusicDateLabel.setBounds(113, 95, 81, 21);
		searchHandlePanel.add(searchEnMusicDateLabel);
		
		txtSearchDate = new JTextField();
		txtSearchDate.setFont(new Font("SimSun", Font.PLAIN, 18));
		txtSearchDate.setColumns(10);
		txtSearchDate.setBounds(201, 92, 150, 27);
		searchHandlePanel.add(txtSearchDate);
		
		JLabel searchEnMusicTypeLabel = new JLabel("\u6B4C\u66F2\u7C7B\u578B:");
		searchEnMusicTypeLabel.setFont(new Font("宋体", Font.PLAIN, 18));
		searchEnMusicTypeLabel.setBounds(570, 95, 81, 21);
		searchHandlePanel.add(searchEnMusicTypeLabel);
		
		txtSearchType = new JTextField();
		txtSearchType.setFont(new Font("SimSun", Font.PLAIN, 18));
		txtSearchType.setColumns(10);
		txtSearchType.setBounds(656, 92, 150, 27);
		searchHandlePanel.add(txtSearchType);
		
		JButton btnSearch = new JButton("\u67E5\u627E");
		btnSearch.setFont(new Font("宋体", Font.PLAIN, 18));
		btnSearch.setBounds(306, 144, 123, 29);
		searchHandlePanel.add(btnSearch);
		
		JButton btnSearchRetset = new JButton("\u91CD\u7F6E");
		btnSearchRetset.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnSearchRetset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtSearchName.setText("");
				txtSearchAuthor.setText("");
				txtSearchDate.setText("");
				txtSearchType.setText("");
			}
		});
		btnSearchRetset.setBounds(504, 144, 123, 29);
		searchHandlePanel.add(btnSearchRetset);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminHandleJDBC handle = new AdminHandleJDBC();
				MusicVO enMusic = new MusicVO();
				if(!txtSearchName.getText().equals(""))
					enMusic.setMname(txtSearchName.getText());
				else
					enMusic.setMname("");
				if(!txtSearchAuthor.getText().equals(""))
					enMusic.setMauthor(txtSearchAuthor.getText());
				else
					enMusic.setMauthor("");
				if(!txtSearchDate.getText().equals(""))
					enMusic.setMtime(txtSearchDate.getText());
				else
					enMusic.setMtime("");
				if(!txtSearchType.getText().equals(""))
					enMusic.setMtype(txtSearchType.getText());
				else
					enMusic.setMtype("");
				ArrayList<MusicVO> en = handle.selectedbyCon(enMusic,getTitle());
				String[] title = {"序号","歌曲号","名称","作者","类型","日期"};
				String[][] values = new String[en.size()][title.length];
				for(int i=0;i<en.size();i++) {
					values[i][0]=i+1+"";
					values[i][1] = en.get(i).getMid()+"";
					values[i][2] = en.get(i).getMname()+"";
					values[i][3] = en.get(i).getMauthor()+"";
					values[i][4] = en.get(i).getMtype()+"";
					values[i][5] = en.get(i).getMtime()+"";
				}
				dtm = new DefaultTableModel(values,title);
				tableInfo.setModel(dtm);
				tableInfo.setRowHeight(20);
				
			}
		});
		
		JPanel addHandlePanel = new JPanel();
		addHandlePanel.setBounds(196, 421, 878, 188);
		groundPanel.add(addHandlePanel);
		addHandlePanel.setLayout(null);
		
		JLabel enMusicNameLabel = new JLabel("\u6B4C\u66F2\u540D\u79F0:");
		enMusicNameLabel.setFont(new Font("SimSun", Font.PLAIN, 18));
		enMusicNameLabel.setBounds(113, 38, 100, 21);
		addHandlePanel.add(enMusicNameLabel);
		
		txtEnMusicName = new JTextField();
		txtEnMusicName.setColumns(10);
		txtEnMusicName.setBounds(201, 35, 150, 27);
		addHandlePanel.add(txtEnMusicName);
		
		JLabel enMusicAuthorLabel = new JLabel("\u4F5C    \u8005:");
		enMusicAuthorLabel.setFont(new Font("SimSun", Font.PLAIN, 18));
		enMusicAuthorLabel.setBounds(570, 41, 90, 21);
		addHandlePanel.add(enMusicAuthorLabel);
		
		txtEnMusicAuthor = new JTextField();
		txtEnMusicAuthor.setColumns(10);
		txtEnMusicAuthor.setBounds(656, 38, 150, 27);
		addHandlePanel.add(txtEnMusicAuthor);
		
		JLabel enMusicDateLabel = new JLabel("\u65E5    \u671F:");
		enMusicDateLabel.setFont(new Font("SimSun", Font.PLAIN, 18));
		enMusicDateLabel.setBounds(113, 95, 81, 21);
		addHandlePanel.add(enMusicDateLabel);
		
		txtEnMusicDate = new JTextField();
		txtEnMusicDate.setBounds(201, 92, 150, 27);
		addHandlePanel.add(txtEnMusicDate);
		txtEnMusicDate.setColumns(10);
		
		JLabel enMusicTypeLabel = new JLabel("\u6B4C\u66F2\u7C7B\u578B:");
		enMusicTypeLabel.setFont(new Font("SimSun", Font.PLAIN, 18));
		enMusicTypeLabel.setBounds(570, 95, 81, 21);
		addHandlePanel.add(enMusicTypeLabel);
		
		txtEnMusicType = new JTextField();
		txtEnMusicType.setBounds(656, 92, 150, 27);
		addHandlePanel.add(txtEnMusicType);
		txtEnMusicType.setColumns(10);
		
		JButton btnAddPass = new JButton("\u786E\u5B9A\u6DFB\u52A0");
		btnAddPass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminHandleJDBC handle = new AdminHandleJDBC();
				MusicVO enMusic = new MusicVO();
				enMusic.setMname(txtEnMusicName.getText());
				enMusic.setMauthor(txtEnMusicAuthor.getText());
				enMusic.setMtime(txtEnMusicDate.getText());
				enMusic.setMtype(txtEnMusicType.getText());
				if(handle.insertData(enMusic,getTitle()))
					JOptionPane.showMessageDialog(null,"添加成功");
				else
					JOptionPane.showMessageDialog(null, "添加失败,请重新尝试");
				look();
			}
		});
		btnAddPass.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnAddPass.setBounds(234, 144, 123, 29);
		addHandlePanel.add(btnAddPass);
		
		JButton btnAddReset = new JButton("\u91CD\u7F6E");
		btnAddReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtEnMusicName.setText("");
				txtEnMusicAuthor.setText("");
				txtEnMusicDate.setText("");
				txtEnMusicType.setText("");
			}
		});
		btnAddReset.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnAddReset.setBounds(453, 144, 123, 29);
		addHandlePanel.add(btnAddReset);
		
		
		JPanel changeHandlePanel = new JPanel();
		changeHandlePanel.setLayout(null);
		changeHandlePanel.setBounds(196, 421, 878, 188);
		groundPanel.add(changeHandlePanel);
		
		JLabel changeEnMusicName = new JLabel("\u4FEE\u6539\u540D\u79F0:");
		changeEnMusicName.setFont(new Font("SimSun", Font.PLAIN, 18));
		changeEnMusicName.setBounds(113, 38, 100, 21);
		changeHandlePanel.add(changeEnMusicName);
		
		txtChangeName = new JTextField();
		txtChangeName.setFont(new Font("宋体", Font.PLAIN, 18));
		txtChangeName.setColumns(10);
		txtChangeName.setBounds(201, 35, 150, 27);
		changeHandlePanel.add(txtChangeName);
		
		JLabel changeEnMusicAuthor = new JLabel("\u4F5C    \u8005:");
		changeEnMusicAuthor.setFont(new Font("SimSun", Font.PLAIN, 18));
		changeEnMusicAuthor.setBounds(570, 41, 90, 21);
		changeHandlePanel.add(changeEnMusicAuthor);
		
		txtChangeAuthor = new JTextField();
		txtChangeAuthor.setFont(new Font("宋体", Font.PLAIN, 18));
		txtChangeAuthor.setColumns(10);
		txtChangeAuthor.setBounds(656, 38, 150, 27);
		changeHandlePanel.add(txtChangeAuthor);
		
		JLabel changeEnMusicDate = new JLabel("\u65E5    \u671F:");
		changeEnMusicDate.setFont(new Font("SimSun", Font.PLAIN, 18));
		changeEnMusicDate.setBounds(113, 95, 81, 21);
		changeHandlePanel.add(changeEnMusicDate);
		
		txtChangeDate = new JTextField();
		txtChangeDate.setFont(new Font("宋体", Font.PLAIN, 18));
		txtChangeDate.setColumns(10);
		txtChangeDate.setBounds(201, 92, 150, 27);
		changeHandlePanel.add(txtChangeDate);
		
		JLabel changeEnMusicType = new JLabel("\u6B4C\u66F2\u7C7B\u578B:");
		changeEnMusicType.setFont(new Font("SimSun", Font.PLAIN, 18));
		changeEnMusicType.setBounds(570, 95, 81, 21);
		changeHandlePanel.add(changeEnMusicType);
		
		txtChangeType = new JTextField();
		txtChangeType.setFont(new Font("宋体", Font.PLAIN, 18));
		txtChangeType.setColumns(10);
		txtChangeType.setBounds(656, 92, 150, 27);
		changeHandlePanel.add(txtChangeType);
		
		JButton btnChangePass = new JButton("\u786E\u5B9A\u4FEE\u6539");
		btnChangePass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminHandleJDBC handle = new AdminHandleJDBC();
				MusicVO enMusic = new MusicVO();
				enMusic.setMid(musicID);
				enMusic.setMname(txtChangeName.getText());
				enMusic.setMauthor(txtChangeAuthor.getText());
				enMusic.setMtime(txtChangeDate.getText());
				enMusic.setMtype(txtChangeType.getText());
				if(handle.changeData(enMusic,getTitle()))
					JOptionPane.showMessageDialog(null,"修改成功");
				else
					JOptionPane.showMessageDialog(null, "修改失败,请重新尝试");
			}
		});
		btnChangePass.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnChangePass.setBounds(234, 144, 123, 29);
		changeHandlePanel.add(btnChangePass);
		
		JButton btnChangeReset = new JButton("\u91CD\u7F6E");
		btnChangeReset.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				txtChangeName.setText("");
				txtChangeAuthor.setText("");
				txtChangeDate.setText("");
				txtChangeType.setText("");
			}
		});
		btnChangeReset.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnChangeReset.setBounds(453, 144, 123, 29);
		changeHandlePanel.add(btnChangeReset);
		
		JPanel delHandlePanel = new JPanel();
		delHandlePanel.setLayout(null);
		delHandlePanel.setBounds(196, 421, 878, 188);
		groundPanel.add(delHandlePanel);
		
		JLabel delEnMusicName = new JLabel("\u6B4C\u66F2\u540D\u79F0:");
		delEnMusicName.setFont(new Font("SimSun", Font.PLAIN, 18));
		delEnMusicName.setBounds(113, 38, 100, 21);
		delHandlePanel.add(delEnMusicName);
		
		txtDelName = new JTextField();
		txtDelName.setFont(new Font("宋体", Font.PLAIN, 18));
		txtDelName.setColumns(10);
		txtDelName.setBounds(201, 35, 150, 27);
		delHandlePanel.add(txtDelName);
		
		JLabel delEnMusicAuthor = new JLabel("\u4F5C    \u8005:");
		delEnMusicAuthor.setFont(new Font("SimSun", Font.PLAIN, 18));
		delEnMusicAuthor.setBounds(570, 41, 90, 21);
		delHandlePanel.add(delEnMusicAuthor);
		
		txtDelAuthor = new JTextField();
		txtDelAuthor.setFont(new Font("宋体", Font.PLAIN, 18));
		txtDelAuthor.setColumns(10);
		txtDelAuthor.setBounds(656, 38, 150, 27);
		delHandlePanel.add(txtDelAuthor);
		
		JLabel delEnMusicDate = new JLabel("\u65E5    \u671F:");
		delEnMusicDate.setFont(new Font("SimSun", Font.PLAIN, 18));
		delEnMusicDate.setBounds(113, 95, 81, 21);
		delHandlePanel.add(delEnMusicDate);
		
		txtDelDate = new JTextField();
		txtDelDate.setFont(new Font("宋体", Font.PLAIN, 18));
		txtDelDate.setColumns(10);
		txtDelDate.setBounds(201, 92, 150, 27);
		delHandlePanel.add(txtDelDate);
		
		JLabel delEnMusicType = new JLabel("\u6B4C\u66F2\u7C7B\u578B:");
		delEnMusicType.setFont(new Font("SimSun", Font.PLAIN, 18));
		delEnMusicType.setBounds(570, 95, 81, 21);
		delHandlePanel.add(delEnMusicType);
		
		txtDelType = new JTextField();
		txtDelType.setFont(new Font("宋体", Font.PLAIN, 18));
		txtDelType.setColumns(10);
		txtDelType.setBounds(656, 92, 150, 27);
		delHandlePanel.add(txtDelType);
		
		JButton btnDelPass = new JButton("\u786E\u5B9A\u5220\u9664");
		btnDelPass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MusicVO enMusic = new MusicVO();
				AdminHandleJDBC handle = new AdminHandleJDBC();
				enMusic.setMid(musicID);
				if(JOptionPane.showConfirmDialog(null, "真的删除吗？")==JOptionPane.YES_OPTION) {
					if(handle.delData(enMusic,getTitle()))
		               JOptionPane.showMessageDialog(null, "删除成功！");
	                else
		               JOptionPane.showMessageDialog(null, "删除失败！");
				}
				look();	
			}
		});
		btnDelPass.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnDelPass.setBounds(306, 144, 123, 29);
		delHandlePanel.add(btnDelPass);
		
		JButton btnDelFlsah = new JButton("\u5237\u65B0");
		btnDelFlsah.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnDelFlsah.setBounds(504, 144, 123, 29);
		delHandlePanel.add(btnDelFlsah);
		
		JPanel HandlePanel = new JPanel();
		HandlePanel.setBounds(15, 15, 166, 594);
		groundPanel.add(HandlePanel);
		HandlePanel.setLayout(null);
		//按钮组件
		JButton lookEnMusic = new JButton("\u67E5\u770B");
		lookEnMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addHandlePanel.setVisible(false);
				changeHandlePanel.setVisible(false);
				delHandlePanel.setVisible(false);
				searchHandlePanel.setVisible(true);
			}
		});
		lookEnMusic.setFont(new Font("SimSun", Font.PLAIN, 18));
		lookEnMusic.setBounds(28, 0, 123, 36);
		HandlePanel.add(lookEnMusic);
		
		JButton addEnMusic = new JButton("\u6DFB\u52A0");
		addEnMusic.setFont(new Font("SimSun", Font.PLAIN, 18));
		addEnMusic.setBounds(28, 84, 123, 36);
		HandlePanel.add(addEnMusic);
		addEnMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addHandlePanel.setVisible(true);
				changeHandlePanel.setVisible(false);
				delHandlePanel.setVisible(false);
				searchHandlePanel.setVisible(false);
			}
		});
		
		JButton changeEnMusic = new JButton("\u4FEE\u6539");
		changeEnMusic.setFont(new Font("SimSun", Font.PLAIN, 18));
		changeEnMusic.setBounds(28, 168, 123, 36);
		HandlePanel.add(changeEnMusic);
		changeEnMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addHandlePanel.setVisible(false);
				changeHandlePanel.setVisible(true);
				delHandlePanel.setVisible(false);
				searchHandlePanel.setVisible(false);
			}
		});
		
		JButton delEnMusic = new JButton("\u5220\u9664");
		delEnMusic.setFont(new Font("SimSun", Font.PLAIN, 18));
		delEnMusic.setBounds(28, 252, 123, 36);
		HandlePanel.add(delEnMusic);
		delEnMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addHandlePanel.setVisible(false);
				changeHandlePanel.setVisible(false);
				delHandlePanel.setVisible(true);
				searchHandlePanel.setVisible(false);
			}
		});
		
		JButton saveEnMusic = new JButton("\u4FDD\u5B58");
		saveEnMusic.setFont(new Font("SimSun", Font.PLAIN, 18));
		saveEnMusic.setBounds(28, 336, 123, 36);
		HandlePanel.add(saveEnMusic);
		
		
		JButton returnEnMusic = new JButton("\u8FD4\u56DE");
		returnEnMusic.setFont(new Font("SimSun", Font.PLAIN, 18));
		returnEnMusic.setBounds(28, 420, 123, 36);
		HandlePanel.add(returnEnMusic);
		returnEnMusic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				close();
			}
		});
	}
	public void close() {
		//this.dispose();
		this.setVisible(false);
	}
	public void look() {
		AdminHandleJDBC handle = new AdminHandleJDBC();
		MusicVO enMusic = new MusicVO();
		enMusic.setMname("");
		enMusic.setMauthor("");
		enMusic.setMtime("");
		enMusic.setMtype("");
		ArrayList<MusicVO> en = handle.selectedbyCon(enMusic,getTitle());
		String[] title = {"序号","歌曲号","名称","作者","类型","日期"};
		String[][] values = new String[en.size()][title.length];
		for(int i=0;i<en.size();i++) {
			values[i][0]=i+1+"";
			values[i][1] = en.get(i).getMid()+"";
			values[i][2] = en.get(i).getMname()+"";
			values[i][3] = en.get(i).getMauthor()+"";
			values[i][4] = en.get(i).getMtype()+"";
			values[i][5] = en.get(i).getMtime()+"";
		}
		dtm = new DefaultTableModel(values,title);
		tableInfo.setModel(dtm);
		tableInfo.setRowHeight(20);
		
	}
}
