package com.andrewx.home;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.geng.database.MysqlLoginCheck;
import com.geng.database.MysqlMusic;
import com.geng.playlist.PlaylistPage;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;

public class LoginPage extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtLoginID;
	private JPasswordField txtLoginPWD;
	private ButtonGroup btnGroup;
	public boolean userFlag = false;
	public boolean adminFlag = false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginPage frame = new LoginPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 497, 335);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtLoginID = new JTextField();
		txtLoginID.setBounds(158, 84, 186, 27);
		contentPane.add(txtLoginID);
		txtLoginID.setColumns(10);
		
		JLabel idLoginLabel = new JLabel("\u8D26\u53F7:");
		idLoginLabel.setFont(new Font("SimSun", Font.PLAIN, 18));
		idLoginLabel.setBounds(104, 87, 51, 21);
		contentPane.add(idLoginLabel);
		
		JLabel pwdLoginLabel = new JLabel("\u5BC6\u7801:");
		pwdLoginLabel.setFont(new Font("SimSun", Font.PLAIN, 18));
		pwdLoginLabel.setBounds(104, 134, 51, 21);
		contentPane.add(pwdLoginLabel);
		
		txtLoginPWD = new JPasswordField();
		txtLoginPWD.setBounds(158, 131, 186, 27);
		contentPane.add(txtLoginPWD);
		
		btnGroup = new ButtonGroup();
		
		JLabel titleLoginLabel = new JLabel("\u6B22\u8FCE\u4F7F\u7528KTV\u70B9\u6B4C\u53F0");
		titleLoginLabel.setForeground(Color.PINK);
		titleLoginLabel.setFont(new Font("楷体", Font.BOLD, 30));
		titleLoginLabel.setBounds(94, 35, 299, 21);
		contentPane.add(titleLoginLabel);
		
		JRadioButton radiobtnLoginUser = new JRadioButton("User");
		radiobtnLoginUser.setFont(new Font("SimSun", Font.PLAIN, 18));
		radiobtnLoginUser.setBounds(120, 179, 87, 29);
		radiobtnLoginUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(radiobtnLoginUser.getText().equals("User")) {
					userFlag = true;
				}
			}
		});
		btnGroup.add(radiobtnLoginUser);
		contentPane.add(radiobtnLoginUser);
		
		JRadioButton radiobtnLoginAdmin = new JRadioButton("Admin");
		radiobtnLoginAdmin.setFont(new Font("SimSun", Font.PLAIN, 18));
		radiobtnLoginAdmin.setBounds(247, 179, 111, 29);
		radiobtnLoginAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(radiobtnLoginAdmin.getText().equals("Admin")) {
					adminFlag = true;
				}
			}
		});
		btnGroup.add(radiobtnLoginAdmin);
		contentPane.add(radiobtnLoginAdmin);
		
		JButton btnLogin = new JButton("\u767B\u5165");
		btnLogin.setFont(new Font("SimSun", Font.PLAIN, 18));
		btnLogin.setBounds(178, 219, 123, 29);
		contentPane.add(btnLogin);
		btnLogin.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				String utype;
				MysqlLoginCheck lc = new MysqlLoginCheck();
				MysqlMusic m = new MysqlMusic();
				if(adminFlag == true) {
					utype="admin";
				}else {
					utype="user";
				}
				if(lc.Logincheck(txtLoginID.getText(),txtLoginPWD.getText(),utype)) {//链接数据库信息
					if(adminFlag == true) {
						HomeKtv admins=new HomeKtv(); // 进入Home界面
						admins.setVisible(true);
						close();
					}
					else if(userFlag == true) {
						m.clearplaylist();
						PlaylistPage users = new PlaylistPage();
						users.setVisible(true);
						close();
					}
				}
				else {
					txtLoginPWD.setText("");
				}
			}
		});
	}
	public void close() {
		//this.dispose();
		this.setVisible(false);
	}
}




